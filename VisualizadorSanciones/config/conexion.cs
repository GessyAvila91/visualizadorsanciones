﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;

namespace VisualizadorSanciones.config {
    
    public class conexion {
        
        private const string _ServerAndroid =
#if CUBOS 
            "MAVICBOSANDROID"
#elif PRODUCCION || PRODDEBUG
            "MAVIANDROID01"
#elif PROSERVER
            "PROSERVER"
#endif
            ;

        private const string _ServerERPMAVI =
#if CUBOS 
            "MAVICUBOS"
#elif PRODUCCION || PRODDEBUG
            "ERPMAVI"
#elif PROSERVER
            "PROSERVER"
#endif
            ;
        
        public conexion() { }

        public void SqlConexionStatica() {
            SqlConnection cnn = new SqlConnection();
            cnn = Conexion.ObjetoConexion.getconexion(_ServerERPMAVI, "INTELISISTMP");
            ClaseEstatica.ConexionEstatica = cnn;
        }

        public void SqlConexionStaticaAndroid() {
            SqlConnection cnn2 = new SqlConnection();
            cnn2 = Conexion.ObjetoConexion.getconexion(_ServerAndroid, "ADMINDOC");
            ClaseEstatica.ConexionEstaticaAndroid = cnn2;
        }

        public void SqlConexionStaticaAndroidServicio() {
            SqlConnection cnn2 = new SqlConnection();
            cnn2 = Conexion.ObjetoConexion.getconexion(_ServerAndroid, "servicioandroid");
            ClaseEstatica.ConexionEstaticaAndroidS = cnn2;
        }

        public void SqlConexionStaticaAndroidSid() {
            SqlConnection cnn3 = new SqlConnection();
            cnn3 = Conexion.ObjetoConexion.getconexion(_ServerAndroid, "SID");
            ClaseEstatica.ConexionEstaticaAndroidSid = cnn3;
        }
    }
}