﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using VisualizadorSanciones.app.model;


namespace VisualizadorSanciones {
    //clase estatica que se pueden usar en toda la aplicacion
    public static class ClaseEstatica {
        /*Conexion*/
        public static SqlConnection ConexionEstatica;

        /*ConexionAndroidServicio*/
        public static SqlConnection ConexionEstaticaAndroidS;

        /*ConexionAndroid*/
        public static SqlConnection ConexionEstaticaAndroid;

        /*ConexionAndroidSid*/
        public static SqlConnection ConexionEstaticaAndroidSid; //-ReporteDima

        /*SPID*/
        public static string SPID;
        
        /*Usuario para toda la aplicacion*/
        public static Usuario Usuario;
        /*Estacion para toda la aplicacion*/
        public static int WorkStation = 0;
        
    }
}