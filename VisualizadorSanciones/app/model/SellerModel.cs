﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Linq;
using System.Reflection;
using VisualizadorSanciones.app.controller.Handler;

namespace VisualizadorSanciones.app.model {
    public class SellerModel {
        
        private string personal;
        private string uen;
        private string nombre;
        private string apellidoPaterno;
        private string apellidoMaterno;
        private string sucursalTrabajo;
        private string estatus;

        public List<SellerModel> sellerList() {
            List<SellerModel> sellerList = new List<SellerModel>();

            string query = @"Select 
            Personal,
            isnull(uen,0),
            Nombre,
            ApellidoPaterno,
            ApellidoMaterno,
            isnull(SucursalTrabajo,0),
            Estatus
                from Personal with (nolock)
				join prop with(nolock)
					on prop.propiedad = personal.personal
                where Estatus = 'ALTA'
				and prop.cuenta like 'ventp%'
            /*AND UEN in (1, 2) ninguno tiene uen*/";

            SqlCommand sqlCommand = new SqlCommand(query, ClaseEstatica.ConexionEstatica);
            sqlCommand.CommandType = CommandType.Text;
            var dataRead = sqlCommand.ExecuteReader();
            
            if (dataRead.HasRows) {
                sellerList = listOFSeller(dataRead);
            }

            return sellerList;
        }

        private List<SellerModel> listOFSeller(SqlDataReader dataRead) {
            List<SellerModel> sellerList = new List<SellerModel>();
            while (dataRead.Read()) {
                sellerList.Add(setSellerValues(dataRead));
            }
            return sellerList;
        }

        private SellerModel setSellerValues(SqlDataReader reader) {
            var seller = new SellerModel();

            seller.personal        = reader.GetString(0) is DBNull ? "Sin Nomina"           : reader.GetString(0);
            seller.uen             = reader.GetInt32(1)  is DBNull ? "0"                    : reader.GetInt32(1).ToString();
            seller.nombre          = reader.GetString(2) is DBNull ? "Sin Nombre"           : reader.GetString(2);
            seller.apellidoPaterno = reader.GetString(3) is DBNull ? "Sin Apellido Paterno" : reader.GetString(3);
            seller.apellidoMaterno = reader.GetString(4) is DBNull ? "Sin Apelldi Materno"  : reader.GetString(4);
            seller.sucursalTrabajo = reader.GetInt32(5)  is DBNull ? "0"                    : reader.GetInt32(5).ToString();
            seller.estatus         = reader.GetString(6) is DBNull ? "SinEstatus"           : reader.GetString(6);

            return seller;
        }

        public String getCbDisplayValue() {
            return personal + " - " + nombre + " " + apellidoPaterno + " " + apellidoMaterno;
        }
        public String getNomina() {
            return personal;
        }
    }
};