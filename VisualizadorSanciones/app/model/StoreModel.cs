﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Linq;
using System.Reflection;
using VisualizadorSanciones.app.controller.Handler;

namespace VisualizadorSanciones.app.model {
    public class StoreModel {
        private string sucursal;
        private string nombre;
        private string wUEN;
        private string estatus;

        public List<StoreModel> storeList() {
            List<StoreModel> storeList = new List<StoreModel>();

            string query = @"Select
                   Sucursal,
                   Nombre,
                   wUEN,
                   Estatus
              from Sucursal with (nolock)
             where wUEN IN (1,2)
               and Estatus = 'ALTA' ";

            try {
                SqlCommand sqlCommand = new SqlCommand(query, ClaseEstatica.ConexionEstatica);
                sqlCommand.CommandType = CommandType.Text;
                var dataRead = sqlCommand.ExecuteReader();
                if (dataRead.HasRows) {
                    storeList = listOFStore(dataRead);
                }
            } catch (Exception ex) {
                DM0312_ErrorLog.RegistraError(
                    MethodBase.GetCurrentMethod().Name.ToString(),
                    MethodBase.GetCurrentMethod().DeclaringType.ToString(), ex);
                MessageBox.Show(ex.Message);
            }

            return storeList;
        }

        private List<StoreModel> listOFStore(SqlDataReader dataRead) {
            List<StoreModel> sellerList = new List<StoreModel>();
            while (dataRead.Read()) {
                sellerList.Add(setStoreValues(dataRead));
            }
            return sellerList;
        }

        private StoreModel setStoreValues(SqlDataReader reader) {
            var store = new StoreModel();

            try {
                store.sucursal = reader.GetInt32(0)  is DBNull ? "0" : reader.GetInt32(0).ToString();
                store.nombre   = reader.GetString(1) is DBNull ? "SinNombre" : reader.GetString(1);
                store.wUEN     = reader.GetInt32(2)  is DBNull ? "0" : reader.GetInt32(2).ToString();
                store.estatus  = reader.GetString(3) is DBNull ? "Sin Estatus" : reader.GetString(3);    
            } catch (Exception ex) {
                DM0312_ErrorLog.RegistraError(
                    MethodBase.GetCurrentMethod().Name.ToString(),
                    MethodBase.GetCurrentMethod().DeclaringType.ToString(), ex);
                MessageBox.Show(ex.Message);
            }
            

            return store;
        }

        public String getCbDisplayValue() {
            return sucursal + " - " + nombre;
        }

        public String getSucursal() {
            return sucursal;
        }
    }
}