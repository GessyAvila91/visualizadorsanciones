﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using VisualizadorSanciones.app.controller.Handler;

using System.ComponentModel;
using System.Drawing;


namespace VisualizadorSanciones.app.model {
    public class SactionTableModel {
        //class Atributes
        public string fecha { get; set; }
        public string sucursal { get; set; }
        public string vendedor { get; set; }
        public string nomina_Vendedor { get; set; }
        public string nombre_Vendedor { get; set; } 
        public string monto { get; set; } 
        public string movID { get; set; } 
        public string cliente { get; set; } 
        public string nombre { get; set; } 
        public string usuarioCredito { get; set; } 
        public string observaciones { get; set; } 
        public string aplica_Bool { get; set; } 
        public string aplica { get; set; } 
        public string estatus { get; set; }

        //condition Filters
        public Dictionary<string, string> parametersFilters = new Dictionary<string, string>();
        
        public DataTable sanctionList() {
            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            
            try {
                using (var cmd = new SqlCommand( getSelectQuery(), ClaseEstatica.ConexionEstatica)) {

                    foreach (var parametro in parametersFilters) {
                        
                        if (parametro.Key != "fromDate" || parametro.Key != "untilDate") {
                            cmd.Parameters.AddWithValue("@" + parametro.Key, parametro.Value);
                        }
                        
                    }
                    using (SqlDataReader dr = cmd.ExecuteReader()) {
                        dataTable.Load(dr);
                    }
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return dataTable;
        }

        private string getSelectQuery() {
            return @"SELECT 
               VS.fecha As 'Fecha',
               VS.sucursal as 'Sucursal',
               VS.vendedor + ' - ' + P.ApellidoPaterno + ' ' + P.ApellidoMaterno + ' ' + P.Nombre as 'Vendedor',
               VS.vendedor as 'Nomina Vendedor',
               P.ApellidoPaterno + ' ' + P.ApellidoMaterno + ' ' + P.Nombre as 'Nombre Vendedor', 
               VS.monto as 'Monto',
               VS.movID as 'ID Venta',
               VS.cliente as 'Cuenta',
               C.Nombre as 'Nombre CTE',
               VS.usuarioCredito as 'Usuario Credito',
               VS.observacion as 'Observacion',
               VS.aplica as 'AplicaBool',
               case when (VS.aplica = 1) then 'SI' else 'NO' end as 'Aplica',
               VS.estatus as 'Estatus'
             FROM VTASCSancion VS with (nolock)
             JOIN Personal P with (nolock) 
                on P.Personal = VS.vendedor
             JOIN CTE C with (nolock) 
                on C.Cliente = VS.cliente " +
                   getWhereClause();
        } 

        public string getWhereClause() {
            string where = "";
            foreach (var parametro in parametersFilters) {
                if (parametro.Key.Equals("fromDate") && validateDate(parametro.Value))
                {
                    where = " where CONVERT(DATE, VS.Fecha) Between @fromDate ";
                }
                if (parametro.Key.Equals("untilDate") && validateDate(parametro.Value)) {
                    where = where + " and @untilDate ";
                }
                
                if ((!parametro.Key.Equals("fromDate") || !parametro.Key.Equals("fromDate")) && !validateDate(parametro.Value) && !parametro.Value.Equals("TODO")) {
                    where = where + "and  vs."+parametro.Key+" = @"+parametro.Key+" ";
                }
                
            }
            return where;
        }
        
        public  bool validateDate(string value) {
            var regex = new Regex(@"^\d{4}\/\d{2}\/\d{2}$");
            return regex.IsMatch(value);
        }
    }
}