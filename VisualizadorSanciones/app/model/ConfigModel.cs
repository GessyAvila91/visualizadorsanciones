﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Linq;
using System.Reflection;
using VisualizadorSanciones.app.controller.Handler;

namespace VisualizadorSanciones.app.model {
    public class ConfigModel {
        
        private string parametro;
        private string valor;
        
        public DataTable configList() {
            List<ConfigModel> configList = new List<ConfigModel>();
            
            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            
            string query = @" 
            Select 
              parametro as Parametro,
              CONVERT(varchar(2), valor) as Valor
              
            from VTASCParametroDeSancion with (nolock)
            ";
            
            try {
                SqlCommand sqlCommand = new SqlCommand(query, ClaseEstatica.ConexionEstatica) {
                    CommandType = CommandType.Text,
                    CommandTimeout = 1200
                };
                sqlDataAdapter.SelectCommand = sqlCommand;
                sqlDataAdapter.Fill(dataTable);
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return dataTable;
        }
        
        public DataTable updateConfigList(String Parametro,Int32 Valor) {
            List<ConfigModel> configList = new List<ConfigModel>();
            
            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            
            string query = @"
            update VTASCParametroDeSancion with (rowlock)
            set valor = @valor 
            where parametro = @parametro
            ";
            
            try {
                SqlCommand sqlCommand = new SqlCommand(query, ClaseEstatica.ConexionEstatica) {
                    CommandType = CommandType.Text,
                    CommandTimeout = 1200
                };
                sqlCommand.Parameters.Add("@valor", SqlDbType.Int);
                sqlCommand.Parameters["@valor"].Value = Valor;
                sqlCommand.Parameters.Add("@parametro", SqlDbType.VarChar);
                sqlCommand.Parameters["@parametro"].Value = Parametro;
                
                sqlCommand.ExecuteNonQuery();
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

            return dataTable;
        }
        
        private List<ConfigModel> listOFConfig(SqlDataReader dataRead) {
            List<ConfigModel> configList = new List<ConfigModel>();
            while (dataRead.Read()) {
                configList.Add(setConfigValues(dataRead));
            }
            return configList;
        }
        private ConfigModel setConfigValues(SqlDataReader reader) {
            var config = new ConfigModel();

            config.parametro = reader.GetString(0) is DBNull ? "ErrorEnParametro" : reader.GetString(0);
            config.valor     = reader.GetString(1) is DBNull ? "ErrorEnValor"     : reader.GetString(1);

            return config;
        }
    }
}