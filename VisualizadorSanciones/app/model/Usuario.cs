﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualizadorSanciones.app.model {
    public class Usuario {
        public string nombre { get; set; }
        public string usuario;
        public int sucursal;
        public string grupoEmpresa;
        public string grupo;
        public int Uen { get; set; }
        public string Acceso { get; set; }

        public string Nombre {
            get { return this.nombre; }
            set { this.nombre = value; }
        }

        public string Usser {
            get { return this.usuario; }
            set { this.usuario = value; }
        }

        public int Sucursal {
            get { return this.sucursal; }
            set { this.sucursal = value; }
        }

        public string GrupoEmpresa {
            get { return this.grupoEmpresa; }
            set { this.grupoEmpresa = value; }
        }

        public string Grupo {
            get { return this.grupo; }
            set { this.grupo = value; }
        }

        public string color;

        //-Datalogic
        public string sRefCtaDinero { get; set; }
        public string sRefCtaCajero { get; set; }
    }
}