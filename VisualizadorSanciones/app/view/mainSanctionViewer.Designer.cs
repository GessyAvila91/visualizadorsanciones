﻿using System;
using System.ComponentModel;

namespace VisualizadorSanciones.app.view {
    partial class mainSanctionViewer {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnl_filtros = new System.Windows.Forms.Panel();
            this.lbl_fromDate = new System.Windows.Forms.Label();
            this.lbl_untilDate = new System.Windows.Forms.Label();
            this.lbl_Store = new System.Windows.Forms.Label();
            this.lbl_Status = new System.Windows.Forms.Label();
            this.lbl_Seller = new System.Windows.Forms.Label();
            this.dtp_fromDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_untilDate = new System.Windows.Forms.DateTimePicker();
            this.cbx_Seller = new System.Windows.Forms.ComboBox();
            this.cbx_Status = new System.Windows.Forms.ComboBox();
            this.cbx_Store = new System.Windows.Forms.ComboBox();
            this.btn_Buscar = new System.Windows.Forms.Button();
            this.btn_excel = new System.Windows.Forms.Button();
            this.btn_configuracion = new System.Windows.Forms.Button();
            this.dgv_sancion = new System.Windows.Forms.DataGridTextBoxColumn();
            this.tlp_main = new System.Windows.Forms.TableLayoutPanel();
            this.dgv_sanctionList = new System.Windows.Forms.DataGridView();
            this.lbl_dgvRowCount = new System.Windows.Forms.Label();
            this.lbl_dgvTotalAmount = new System.Windows.Forms.Label();
            this.lbl_SanctionSelect = new System.Windows.Forms.Label();
            
            this.tlp_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sanctionList)).BeginInit();
            this.pnl_filtros.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_filtros
            // 
            this.pnl_filtros.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.pnl_filtros.AutoSize = true;
            
            this.pnl_filtros.Controls.Add(this.lbl_fromDate);
            this.pnl_filtros.Controls.Add(this.dtp_fromDate);
            this.pnl_filtros.Controls.Add(this.lbl_untilDate);
            this.pnl_filtros.Controls.Add(this.dtp_untilDate);
            this.pnl_filtros.Controls.Add(this.lbl_Seller);
            this.pnl_filtros.Controls.Add(this.cbx_Seller);
            this.pnl_filtros.Controls.Add(this.lbl_Status);
            this.pnl_filtros.Controls.Add(this.cbx_Status);
            this.pnl_filtros.Controls.Add(this.lbl_Store);
            this.pnl_filtros.Controls.Add(this.cbx_Store);
            
            this.pnl_filtros.Controls.Add(this.btn_configuracion);
            this.pnl_filtros.Controls.Add(this.btn_Buscar);
            this.pnl_filtros.Controls.Add(this.btn_excel);
            
            this.pnl_filtros.Location = new System.Drawing.Point(3, 3);
            this.pnl_filtros.MaximumSize = new System.Drawing.Size(9999, 89);
            this.pnl_filtros.MinimumSize = new System.Drawing.Size(1142, 89);
            this.pnl_filtros.Name = "pnl_filtros";
            this.pnl_filtros.Size = new System.Drawing.Size(1142, 89);
            this.pnl_filtros.TabIndex = 0;
            // 
            // lbl_fromDate
            // 
            this.lbl_fromDate.Location = new System.Drawing.Point(3, 5);
            this.lbl_fromDate.Name = "lbl_fromDate";
            this.lbl_fromDate.Size = new System.Drawing.Size(82, 20);
            this.lbl_fromDate.TabIndex = 1;
            this.lbl_fromDate.Text = "Fecha (D):";
            this.lbl_fromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtp_fromDate
            // 
            this.dtp_fromDate.CustomFormat = "dd/MM/yyyy";
            this.dtp_fromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_fromDate.Location = new System.Drawing.Point(91, 3);
            this.dtp_fromDate.MaxDate = new System.DateTime(2021, 7, 16, 23, 59, 59, 0);
            this.dtp_fromDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtp_fromDate.Name = "dtp_fromDate";
            this.dtp_fromDate.Size = new System.Drawing.Size(130, 20);
            this.dtp_fromDate.TabIndex = 2;
            this.dtp_fromDate.Value = new System.DateTime(2021, 7, 8, 0, 0, 0, 0);
            // 
            // lbl_untilDate
            // 
            this.lbl_untilDate.Location = new System.Drawing.Point(448, 5);
            this.lbl_untilDate.Name = "lbl_untilDate";
            this.lbl_untilDate.Size = new System.Drawing.Size(43, 20);
            this.lbl_untilDate.TabIndex = 3;
            this.lbl_untilDate.Text = "A:";
            this.lbl_untilDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtp_untilDate
            // 
            this.dtp_untilDate.CustomFormat = "dd/MM/yyyy";
            this.dtp_untilDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_untilDate.Location = new System.Drawing.Point(546, 3);
            this.dtp_untilDate.MaxDate = new System.DateTime(2021, 7, 16, 23, 59, 59, 0);
            this.dtp_untilDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtp_untilDate.Name = "dtp_untilDate";
            this.dtp_untilDate.Size = new System.Drawing.Size(130, 20);
            this.dtp_untilDate.TabIndex = 4;
            this.dtp_untilDate.Value = new System.DateTime(2021, 7, 8, 0, 0, 0, 0);
            // 
            // lbl_Seller
            // 
            this.lbl_Seller.Location = new System.Drawing.Point(3, 29);
            this.lbl_Seller.Name = "lbl_Seller";
            this.lbl_Seller.Size = new System.Drawing.Size(82, 21);
            this.lbl_Seller.TabIndex = 5;
            this.lbl_Seller.Text = "Vendedor:";
            this.lbl_Seller.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbx_Seller
            //
            this.cbx_Seller.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbx_Seller.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbx_Seller.FormattingEnabled = true;
            this.cbx_Seller.Location = new System.Drawing.Point(91, 29);
            this.cbx_Seller.Name = "cbx_Seller";
            this.cbx_Seller.Size = new System.Drawing.Size(351, 21);
            this.cbx_Seller.TabIndex = 6;
            // 
            // lbl_Status
            // 
            this.lbl_Status.Location = new System.Drawing.Point(448, 29);
            this.lbl_Status.Name = "lbl_Status";
            this.lbl_Status.Size = new System.Drawing.Size(92, 20);
            this.lbl_Status.TabIndex = 7;
            this.lbl_Status.Text = "Estatus:";
            this.lbl_Status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbx_Status
            // 
            this.cbx_Status.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbx_Status.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbx_Status.FormattingEnabled = true;
            this.cbx_Status.Location = new System.Drawing.Point(546, 29);
            this.cbx_Status.Name = "cbx_Status";
            this.cbx_Status.Size = new System.Drawing.Size(130, 21);
            this.cbx_Status.TabIndex = 8;
            // 
            // lbl_Store
            // 
            this.lbl_Store.Location = new System.Drawing.Point(3, 56);
            this.lbl_Store.Name = "lbl_Store";
            this.lbl_Store.Size = new System.Drawing.Size(82, 21);
            this.lbl_Store.TabIndex = 10;
            this.lbl_Store.Text = "Sucursal:";
            this.lbl_Store.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbx_Store
            // 
            this.cbx_Store.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbx_Store.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbx_Store.FormattingEnabled = true;
            this.cbx_Store.Location = new System.Drawing.Point(91, 56);
            this.cbx_Store.Name = "cbx_Store";
            this.cbx_Store.Size = new System.Drawing.Size(351, 21);
            this.cbx_Store.TabIndex = 11;
            // 
            // btn_configuracion
            // 
            this.btn_configuracion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_configuracion.Location = new System.Drawing.Point(1043, 3);
            this.btn_configuracion.Name = "btn_configuracion";
            this.btn_configuracion.Size = new System.Drawing.Size(96, 25);
            this.btn_configuracion.TabIndex = 12;
            this.btn_configuracion.Text = "Configuración";
            this.btn_configuracion.UseVisualStyleBackColor = true;
            this.btn_configuracion.Click += new System.EventHandler(this.btn_configView_Click);
            // 
            // btn_Buscar
            // 
            this.btn_Buscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Buscar.Location = new System.Drawing.Point(958, 56);
            this.btn_Buscar.Name = "btn_Buscar";
            this.btn_Buscar.Size = new System.Drawing.Size(79, 25);
            this.btn_Buscar.TabIndex = 13;
            this.btn_Buscar.Text = "Buscar";
            this.btn_Buscar.UseVisualStyleBackColor = true;
            this.btn_Buscar.Click += new System.EventHandler(this.btn_Buscar_Click);
            // 
            // btn_excel
            // 
            this.btn_excel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_excel.Image = global::VisualizadorSanciones.Properties.Resources.MS_Excel_Icon_24x24;
            this.btn_excel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_excel.Location = new System.Drawing.Point(1043, 56);
            this.btn_excel.Name = "btn_excel";
            this.btn_excel.Size = new System.Drawing.Size(96, 25);
            this.btn_excel.TabIndex = 14;
            this.btn_excel.Text = "EXCEL";
            this.btn_excel.UseVisualStyleBackColor = true;
            this.btn_excel.Click += new System.EventHandler(this.btn_excel_Click);
            // 
            // dgv_sancion
            // 
            this.dgv_sancion.Format = "";
            this.dgv_sancion.FormatInfo = null;
            this.dgv_sancion.Width = -1;
            // 
            // lbl_dgvRowCount
            // 
            this.lbl_dgvRowCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_dgvRowCount.Location = new System.Drawing.Point(12, 418);
            this.lbl_dgvRowCount.Name = "lbl_dgvRowCount";
            this.lbl_dgvRowCount.Size = new System.Drawing.Size(221, 23);
            this.lbl_dgvRowCount.TabIndex = 15;
            this.lbl_dgvRowCount.Text = "Total Registros: 0";
            this.lbl_dgvRowCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_dgvTotalAmount
            // 
            this.lbl_dgvTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_dgvTotalAmount.Location = new System.Drawing.Point(466, 418);
            this.lbl_dgvTotalAmount.Name = "lbl_dgvTotalAmount";
            this.lbl_dgvTotalAmount.Size = new System.Drawing.Size(221, 23);
            this.lbl_dgvTotalAmount.TabIndex = 16;
            this.lbl_dgvTotalAmount.Text = "Monto Total Sanciónes: $0.0000";
            this.lbl_dgvTotalAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_SanctionSelect
            // 
            this.lbl_SanctionSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_SanctionSelect.Location = new System.Drawing.Point(239, 418);
            this.lbl_SanctionSelect.Name = "lbl_SanctionSelect";
            this.lbl_SanctionSelect.Size = new System.Drawing.Size(221, 23);
            this.lbl_SanctionSelect.TabIndex = 17;
            this.lbl_SanctionSelect.Text = "Sanción Aplicada: $0.0000";
            this.lbl_SanctionSelect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgv_sanctionList
            // 
            this.dgv_sanctionList.AllowUserToAddRows = false;
            this.dgv_sanctionList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_sanctionList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_sanctionList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_sanctionList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_sanctionList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_sanctionList.Location = new System.Drawing.Point(3, 89);
            this.dgv_sanctionList.Name = "dgv_sanctionList";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_sanctionList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_sanctionList.Size = new System.Drawing.Size(1142, 311);
            this.dgv_sanctionList.TabIndex = 18;
            // 
            // tlp_main
            // 
            this.tlp_main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.tlp_main.ColumnCount = 1;
            this.tlp_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_main.Controls.Add(this.dgv_sanctionList, 0, 1);
            this.tlp_main.Controls.Add(this.pnl_filtros, 0, 0);
            this.tlp_main.Location = new System.Drawing.Point(15, 12);
            this.tlp_main.Name = "tlp_main";
            this.tlp_main.RowCount = 2;
            this.tlp_main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.tlp_main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_main.Size = new System.Drawing.Size(1148, 403);
            this.tlp_main.TabIndex = 19;
            // 
            // mainSanctionViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1166, 450);
            this.Controls.Add(this.tlp_main);
            this.Controls.Add(this.lbl_SanctionSelect);
            this.Controls.Add(this.lbl_dgvTotalAmount);
            this.Controls.Add(this.lbl_dgvRowCount);
            this.Location = new System.Drawing.Point(15, 15);
            this.MinimumSize = new System.Drawing.Size(1182, 489);
            this.Name = "mainSanctionViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.tlp_main.ResumeLayout(false);
            this.tlp_main.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sanctionList)).EndInit();
            this.pnl_filtros.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.Button btn_Buscar;

        private System.Windows.Forms.DataGridView dgv_sanctionList;

        private System.Windows.Forms.Button button3;

        private System.Windows.Forms.Panel pnl_filtros;

        private System.Windows.Forms.TableLayoutPanel tlp_main;

        private System.Windows.Forms.Label lbl_SanctionSelect;

        private System.Windows.Forms.Label lbl_dgvTotalAmount;

        private System.Windows.Forms.Label lbl_dgvRowCount;
        private System.Windows.Forms.Label label5;

        private System.Windows.Forms.ComboBox cbx_Seller;
        private System.Windows.Forms.ComboBox cbx_Store;
        private System.Windows.Forms.ComboBox cbx_Status;

        private System.Windows.Forms.Label label4;

        private System.Windows.Forms.DataGridTextBoxColumn dgv_sancion;

        private System.Windows.Forms.Label lbl_Seller;
        private System.Windows.Forms.Label lbl_Store;
        private System.Windows.Forms.Label lbl_Status;
        
        private System.Windows.Forms.Button btn_configuracion;
        private System.Windows.Forms.Button btn_excel;

        private System.Windows.Forms.Label lbl_fromDate;
        private System.Windows.Forms.Label lbl_untilDate;

        private System.Windows.Forms.DateTimePicker dtp_fromDate;

        private System.Windows.Forms.DateTimePicker dtp_untilDate;

        private System.Windows.Forms.Label lbl_DateStart;
        
        #endregion
    }
}