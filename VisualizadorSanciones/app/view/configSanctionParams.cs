﻿using System;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using VisualizadorSanciones.app.model;

namespace VisualizadorSanciones.app.view {
    public partial class configSanctionParams : Form {
        ConfigModel CM = new ConfigModel();

        public configSanctionParams() {
            InitializeComponent();
            configTableInit();
        }

        private void configTableInit() {


            dgv_configList.DataSource = null;
            dgv_configList.DataSource = CM.configList();

            dgv_configList.Columns["Parametro"].ReadOnly = true;
            ((DataGridViewTextBoxColumn)dgv_configList.Columns["Valor"]).MaxInputLength = 2;

        }

        private void btn_guardar_Click(object sender, EventArgs e) {
            bool result = true;

            foreach(DataGridViewRow row in dgv_configList.Rows) {

                if(isANumber(row.Cells[1].Value.ToString())) {
                    CM.updateConfigList(row.Cells[0].Value.ToString(), Int32.Parse(row.Cells[1].Value.ToString()));
                }
                else {
                    MessageBox.Show("el valor " + row.Cells[1].Value.ToString() + " no es numerico");
                    result = false;
                }
            }

            if(result) {
                Close();
            }
        }

        private bool isANumber(string valor) {
            return int.TryParse(valor, out _);
        }

        private void dgv_configList_KeyDown(object sender, KeyEventArgs e) { }
    }
}