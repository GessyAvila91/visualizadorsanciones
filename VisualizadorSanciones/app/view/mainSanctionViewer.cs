﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using VisualizadorSanciones.app.controller.Handler;
using VisualizadorSanciones.app.model;

namespace VisualizadorSanciones.app.view {
    public partial class mainSanctionViewer : Form {
        
        private SellerModel sellerM = new SellerModel();
        private StoreModel storeM = new StoreModel();
        private SactionTableModel SactionTableModel = new SactionTableModel();
        
        private Dictionary<string, string> filters = new Dictionary<string, string>();
        
        sqlHandler sql = new sqlHandler();

        private class cbValue {
            public string Value { get; set; }
            public string Index { get; set; }
        }

        public mainSanctionViewer() {
            InitializeComponent();
            InitializeComponentValues();
        }

        //Init Values
        private void InitializeComponentValues() {
            
            DateTime serverDateTime = sql.serverDateTime() != null ? sql.serverDateTime() : DateTime.Today;
            // dtp_fromDate
            dtp_fromDate.MaxDate = serverDateTime;
            dtp_fromDate.Value = serverDateTime.AddDays(-7);

            // dtp_untilDate
            dtp_untilDate.MaxDate = serverDateTime;
            dtp_untilDate.Value = serverDateTime;

            //cbx_Seller
            cbx_SellerFiller();
            //cbx_Store
            cbx_StoreFiller();
            //cbx_Status
            cbx_StatusFiller();
        }

        #region InitView

        private void cbx_SellerFiller() {
            List<cbValue> sellerCBValues = new List<cbValue>();
            List<SellerModel> sellerList = sellerM.sellerList();

            sellerCBValues.Add(new cbValue() {Index = "TODO", Value = "TODO"});

            sellerList.ForEach(delegate(SellerModel seller) {
                sellerCBValues.Add(new cbValue() {Index = seller.getNomina(), Value = seller.getCbDisplayValue()});
            });

            CBfiller(cbx_Seller, sellerCBValues);
        }

        private void cbx_StoreFiller() {
            List<cbValue> storeCBValues = new List<cbValue>();
            List<StoreModel> storeList = storeM.storeList();

            storeCBValues.Add(new cbValue() {Index = "TODO", Value = "TODO"});
            storeList.ForEach(delegate(StoreModel store) {
                storeCBValues.Add(new cbValue() {Index = store.getSucursal(), Value = store.getCbDisplayValue()});
            });

            CBfiller(cbx_Store, storeCBValues);
        }

        private void cbx_StatusFiller() {
            var valores = new List<cbValue>();

            valores.Add(new cbValue() {Index = "TODO", Value = "TODO"});
            valores.Add(new cbValue() {Index = "ENVIADO", Value = "ENVIADO"});
            valores.Add(new cbValue() {Index = "PENDIENTE", Value = "PENDIENTE"});

            CBfiller(cbx_Status, valores);
        }

        #endregion

        #region trigger

        private void btn_Buscar_Click(object sender, EventArgs e) {
            fetchSanctionAndFill();
        }

        private void btn_excel_Click(object sender, EventArgs e) {
            if (dgv_sanctionList.Rows.Count > 0) {
                var columnList = ColumnList();
                List<string> tituloExcel = new List<string>(new [] {
                    "MAVI DE OCCIDENTE S.A. DE C.V.",
                    "REPORTE DE SANCIONES POR ERROR DE REACTIVACIÓN",
                    DateTime.Now.ToString("dd/MM/yyyy h:mm tt")
                });
                
                excelExporter eE = new excelExporter(dgv_sanctionList,columnList,tituloExcel);
                eE.ExportarDgvAExcel();
            }
            else {
                MessageBox.Show("sin Registros en la Tabla para exportar", "Error", MessageBoxButtons.OK);
            }
        }
        private List<string> ColumnList() {
            List<string> columnList = new List<string>();
            foreach (DataGridViewColumn column in dgv_sanctionList.Columns) {
                if(column.Visible)
                    columnList.Add(column.Name);
            }    
            return columnList;
        }

        private void btn_configView_Click(object sender, EventArgs e) {
            using(configSanctionParams configView = new configSanctionParams()) {
                configView.ShowDialog();    
            }
        }

        #endregion

        #region Logic

        private void fetchSanctionAndFill() {
            if (paramsValidate()) {
                setFilters();

                SactionTableModel = new SactionTableModel();
                SactionTableModel.parametersFilters = filters;

                dgv_sanctionList.DataSource = null;
                dgv_sanctionList.DataSource = SactionTableModel.sanctionList();

                if (dgv_sanctionList.RowCount > 0) {
                    if (dgv_sanctionList.Columns["Monto"] != null) 
                        dgv_sanctionList.Columns["Monto"].DefaultCellStyle.Format = "C4";
                    
                    if (dgv_sanctionList.Columns["Vendedor"] != null) 
                        dgv_sanctionList.Columns["Vendedor"].Visible = false;
                    
                    if (dgv_sanctionList.Columns["AplicaBool"] != null) 
                        dgv_sanctionList.Columns["AplicaBool"].Visible = false;
                    
                    setInfoLbls();
                }
                else {
                    MessageBox.Show("Registros no encontrados", "Error");
                    setInfoLbls();
                }
            }
        }

        private void CBfiller(ComboBox CB, List<cbValue> Values) {
            CB.DataSource = Values;
            CB.DisplayMember = "Value";
            CB.ValueMember = "Index";
        }

        private bool paramsValidate() {
            bool flag = true;
            string msg = "";
            
            try {
                
                if (cbx_Seller.SelectedIndex < 0) {
                    cbx_Seller.SelectedIndex = 0;
                }
                if (cbx_Status.SelectedIndex < 0) {
                    cbx_Status.SelectedIndex = 0;
                }

                if (cbx_Store.SelectedIndex < 0) {
                    cbx_Store.SelectedIndex = 0;
                }
                
                var seller = (cbValue) cbx_Seller.SelectedItem;
                var status = (cbValue) cbx_Status.SelectedItem;
                var store = (cbValue) cbx_Store.SelectedItem;

                if (!validaParametrosDeFecha()) {
                    msg = msg + "Fecha 'A' no debe ser menor a Fecha 'DE'\n";
                    flag = false;
                }

                if (string.IsNullOrEmpty(seller.Value)) {
                    msg = msg + "Error en el Vendedor\n";
                    flag = false;
                }

                if (string.IsNullOrEmpty(status.Value)) {
                    msg = msg + "Error en el Status\n";
                    flag = false;
                }

                if (string.IsNullOrEmpty(store.Value)) {
                    msg = msg + "Error en el Tienda\n";
                    flag = false;
                }

                if (!string.IsNullOrEmpty(msg)) {
                    MessageBox.Show(msg, "Error en Filtros");
                }
            }
            catch (Exception e) {
                DM0312_ErrorLog.RegistraError(MethodBase.GetCurrentMethod().Name,
                    MethodBase.GetCurrentMethod().DeclaringType?.Name, e);
                //sbp_EstatusPrograma.Text = @"Error: " + e.Message;
                throw new Exception(
                    "CLASE: " + MethodBase.GetCurrentMethod().DeclaringType?.Name + "\nMETODO: " +
                    MethodBase.GetCurrentMethod().Name + "\nMessage:" + e.Message, e);
            }

            return flag;
        }

        private bool validaParametrosDeFecha() {
            return DateTime.Compare(dtp_fromDate.Value, dtp_untilDate.Value) <= 0 ? true : false;
        }

        private void setFilters() {
            filters = new Dictionary<string, string>();

            var seller = (cbValue) cbx_Seller.SelectedItem;
            var status = (cbValue) cbx_Status.SelectedItem;
            var store = (cbValue) cbx_Store.SelectedItem;

            filters.Add("fromDate", dtp_fromDate.Value.ToString("yyyy/MM/dd"));
            filters.Add("untilDate", dtp_untilDate.Value.ToString("yyyy/MM/dd"));
            filters.Add("vendedor", seller.Index);
            filters.Add("estatus", status.Index);
            filters.Add("sucursal", store.Index);
        }

        private void setInfoLbls() {
            double total = 0.0;
            double sancionAplicada = 0.0;

            foreach (DataGridViewRow r in dgv_sanctionList.Rows) {
                total += Convert.ToDouble(r.Cells["Monto"].Value);
                if (r.Cells["Aplica"].Value.ToString() == "SI" && r.Cells["Estatus"].Value.ToString() == "ENVIADO") {
                    sancionAplicada += Convert.ToDouble(r.Cells["Monto"].Value);    
                }
            }
            
            lbl_dgvTotalAmount.Text = dgv_sanctionList.RowCount > 0 
                ? "Monto Total Sanciónes: " + total.ToString("C4")
                : "Monto Total Sanciónes: $0.0000";
            
            lbl_dgvRowCount.Text = dgv_sanctionList.RowCount > 0
                ? "Total Registros: " + dgv_sanctionList.RowCount.ToString()
                : "Total Registros: 0";
            
            lbl_SanctionSelect.Text = dgv_sanctionList.RowCount > 0
                ? "Sanción Aplicada: " + sancionAplicada.ToString("C4")
                : "Sanción Aplicada: $0.0000";
        }

        #endregion
    }
}