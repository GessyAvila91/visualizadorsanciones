﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Windows.Forms;

namespace VisualizadorSanciones.app.controller.Handler {
    public class DM0312_ErrorLog {
        public static void RegistraError(string metodo, string clase, Exception ex) {
            var ip = Dns.GetHostEntry(Dns.GetHostName()).AddressList
                .First(f => f.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString();
            try {
                SqlCommand command = new SqlCommand("SP_TREDM0312_ErrorLog", ClaseEstatica.ConexionEstatica);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@UsuarioSo", Environment.UserName.ToString());
                command.Parameters.AddWithValue("@So", Environment.OSVersion.ToString());
                command.Parameters.AddWithValue("@Ip", ip);
                command.Parameters.AddWithValue("@Usuario",
                    ClaseEstatica.Usuario != null ? ClaseEstatica.Usuario.Usser : "");
                command.Parameters.AddWithValue("@Acceso",
                    ClaseEstatica.Usuario != null ? ClaseEstatica.Usuario.Acceso : "");
                command.Parameters.AddWithValue("@Sucursal",
                    ClaseEstatica.Usuario != null ? ClaseEstatica.Usuario.Sucursal : 0);
                command.Parameters.AddWithValue("@Estacion",
                    ClaseEstatica.WorkStation != 0 ? ClaseEstatica.WorkStation : 0);
                command.Parameters.AddWithValue("@Metodo", metodo);
                command.Parameters.AddWithValue("@Clase", clase);
                command.Parameters.AddWithValue("@MensajeError", ex.Message);
                command.Parameters.AddWithValue("@TipoError", ex.GetType().ToString());
                command.Parameters.AddWithValue("@StackTrace", ex.StackTrace);
                command.Parameters.AddWithValue("@Sourse", ex.Source);
                command.Parameters.AddWithValue("@Fecha", DateTime.Now);
                command.ExecuteNonQuery();
            } catch (Exception e) {
                DM0312_ErrorLog.RegistraError(MethodBase.GetCurrentMethod().Name,
                    MethodBase.GetCurrentMethod().DeclaringType.Name, e);
                MessageBox.Show(ex.Message);
            }
        }
    }
}