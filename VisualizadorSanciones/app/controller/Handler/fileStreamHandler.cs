﻿using System;
using System.Reflection;
using System.Windows.Forms;

namespace VisualizadorSanciones.app.controller.Handler {
    public class fileStreamHandler {
        SaveFileDialog SFD = new SaveFileDialog();

        const string defaultFileName = "file";
        const string defaultExtension = ".file";

        string defaultPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

        public string pathToStoreFile(string path, string fileName, fileType fileType) {
            try {
                SFD.Filter = getFilterString(fileType);
                SFD.Title = "Guardar archivo en...";
                SFD.FileName = String.IsNullOrEmpty(fileName)
                    ? defaultFileName + defaultExtension
                    : fileName + getExtencionString(fileType);

                SFD.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                SFD.ShowDialog();

                if (String.IsNullOrEmpty(SFD.FileName)) {
                    SFD.FileName = defaultPath + '/' + defaultFileName;
                }
            } catch (Exception exception) {
                DM0312_ErrorLog.RegistraError(MethodBase.GetCurrentMethod().Name,
                    MethodBase.GetCurrentMethod().DeclaringType.Name, exception);
                MessageBox.Show(exception.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //TODO agregar opciones de cancelado y cerrado

            return SFD.FileName;
        }

        public enum fileType {
            Word,
            Excel,
            PowerPoint,
            Office,
            All
        }

        private string getFilterString(fileType fetch) {
            switch (fetch) {
                case fileType.Word:
                    return "Word Documents|*.doc";
                case fileType.Excel:
                    return "Excel Worksheets|*.xlsx";
                case fileType.PowerPoint:
                    return "PowerPoint Presentations|*.ppt";
                case fileType.Office:
                    return "Office Files|*.doc;*.xlsx;*.ppt";
                case fileType.All:
                    return "All Files|*.*";
                default:
                    return "All Files|*.*";
            }
        }

        private string getExtencionString(fileType fetch) {
            switch (fetch) {
                case fileType.Word:
                    return ".doc";
                case fileType.Excel:
                    return ".xlsx";
                case fileType.PowerPoint:
                    return ".ppt";
                default:
                    return "NO VALUE GIVEN";
            }
        }
    }
}