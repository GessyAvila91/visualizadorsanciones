﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace VisualizadorSanciones.app.controller.Handler {
    public class excelExporter {
        private List<string> columnList;
        private List<string> tituloExcel;
        private DataGridView dgv;
        
        public excelExporter(DataGridView Dgv,List<string> ColumnList, List<string> TituloExcel) {
            dgv = Dgv;
            columnList = ColumnList;
            tituloExcel = TituloExcel;    
        }

        public void ExportarDgvAExcel() {
            
            using(SpreadsheetDocument myWorkbook = SpreadsheetDocument.Create(getFileNameAndPath(), SpreadsheetDocumentType.Workbook)){

                List<OpenXmlElement> renglones = new List<OpenXmlElement>();
                
                //Titulos
                if (tituloExcel.Count>0)
                    getTitulos(renglones);

                //Heads
                RenglonCabeceras(renglones);
                
                //Datos
                dataCell(renglones);
                
                //Libro
                createSheet(myWorkbook, renglones);
            }
        }
        
        private void getTitulos(List<OpenXmlElement> renglones) {
            try {
                foreach (string titulo in tituloExcel) {
                    Cell celdas = new Cell();
                    celdas.DataType = CellValues.String;
                    celdas.CellValue = new CellValue(titulo);

                    Row renglon = new Row();
                    renglon.Append(celdas);
                    renglones.Add(renglon);
                }
            }
            catch (Exception e) {
                DM0312_ErrorLog.RegistraError(MethodBase.GetCurrentMethod().Name,
                    MethodBase.GetCurrentMethod().DeclaringType?.Name, e);
                throw new Exception(
                    "CLASE: " + MethodBase.GetCurrentMethod().DeclaringType?.Name + "\nMETODO: " +
                    MethodBase.GetCurrentMethod().Name + "\nMessage:" + e.Message, e);
            }
        }
        private void RenglonCabeceras(List<OpenXmlElement> renglones) {
            try {
                Row renglonCabeceras = new Row();
                foreach (DataGridViewColumn column in dgv.Columns) {
                    if (columnList.Contains(column.Name)) {
                        Cell cellsHeader = new Cell();
                        cellsHeader.DataType = CellValues.String;
                        cellsHeader.CellValue = new CellValue(column.Name);
                        renglonCabeceras.Append(cellsHeader);
                    }
                    
                }
                renglones.Add(renglonCabeceras);
            }
            catch (Exception e) {
                DM0312_ErrorLog.RegistraError(MethodBase.GetCurrentMethod().Name,
                    MethodBase.GetCurrentMethod().DeclaringType?.Name, e);
                throw new Exception(
                    "CLASE: " + MethodBase.GetCurrentMethod().DeclaringType?.Name + "\nMETODO: " +
                    MethodBase.GetCurrentMethod().Name + "\nMessage:" + e.Message, e);
            }
        }
        private void dataCell(List<OpenXmlElement> renglones) {

            try {
                foreach (DataGridViewRow row in dgv.Rows) {
                    Row renglonData = new Row();
                    foreach (DataGridViewCell cells in row.Cells) {
                        if (columnList.Contains(dgv.Columns[cells.ColumnIndex].Name)) {
                            Cell cellsData = new Cell();
                            cellsData.DataType = CellValues.String;
                            cellsData.CellValue = new CellValue(cells.Value.ToString());
                            renglonData.Append(cellsData);   
                        }
                    }
                    renglones.Add(renglonData);
                }
            }
            catch (Exception e) {
                DM0312_ErrorLog.RegistraError(MethodBase.GetCurrentMethod().Name,
                    MethodBase.GetCurrentMethod().DeclaringType?.Name, e);
                throw new Exception(
                    "CLASE: " + MethodBase.GetCurrentMethod().DeclaringType?.Name + "\nMETODO: " +
                    MethodBase.GetCurrentMethod().Name + "\nMessage:" + e.Message, e);
            }
        }
        private static void createSheet(SpreadsheetDocument myWorkbook, List<OpenXmlElement> renglones) {
            try {
                SheetData sheetData = new SheetData(renglones);
            
                WorkbookPart workbookpart = myWorkbook.AddWorkbookPart();
                WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                //Hojas
                Sheet sheet = new Sheet {
                    Name = "reporte",
                    Id = myWorkbook.WorkbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1
                };
            
                Sheets sheets = new Sheets();
                sheets.Append(sheet);
            
                // Agregar la hoja al documento y guardar
                Workbook workbook = new Workbook();
                workbook.Append(sheets);
                Worksheet worksheet = new Worksheet();
                worksheet.Append(sheetData);
                worksheetPart.Worksheet = worksheet;
                worksheetPart.Worksheet.Save();
                myWorkbook.WorkbookPart.Workbook = workbook;
                myWorkbook.WorkbookPart.Workbook.Save();
                myWorkbook.Close();
            }
            catch (Exception e) {
                DM0312_ErrorLog.RegistraError(MethodBase.GetCurrentMethod().Name,
                    MethodBase.GetCurrentMethod().DeclaringType?.Name, e);
                //sbp_EstatusPrograma.Text = @"Error: " + e.Message;
                throw new Exception(
                    "CLASE: " + MethodBase.GetCurrentMethod().DeclaringType?.Name + "\nMETODO: " +
                    MethodBase.GetCurrentMethod().Name + "\nMessage:" + e.Message, e);
            }
        }
        private string getFileNameAndPath() {
            try {
                fileStreamHandler FSH = new fileStreamHandler();

                string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                string fileName = "Sanciones " + DateTime.Now.ToString("ddMMyyyyhmm");

                return FSH.pathToStoreFile(path, fileName, fileStreamHandler.fileType.Excel);
            }
            catch (Exception e) {
                DM0312_ErrorLog.RegistraError(MethodBase.GetCurrentMethod().Name,
                    MethodBase.GetCurrentMethod().DeclaringType?.Name, e);
                //sbp_EstatusPrograma.Text = @"Error: " + e.Message;
                throw new Exception(
                    "CLASE: " + MethodBase.GetCurrentMethod().DeclaringType?.Name + "\nMETODO: " +
                    MethodBase.GetCurrentMethod().Name + "\nMessage:" + e.Message, e);
            }
        }
    }
}