﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using VisualizadorSanciones.app.controller;
using VisualizadorSanciones.app.controller.Handler;

namespace VisualizadorSanciones{
    public class sqlHandler {
        
        public DateTime serverDateTime() {
            DateTime result = new DateTime();
            string query = "Select getDate() as dateTime";
            
            SqlCommand sqlCommand = new SqlCommand(query, ClaseEstatica.ConexionEstatica);
            sqlCommand.CommandType = CommandType.Text;

            var dataRead = sqlCommand.ExecuteReader();

            if (dataRead.HasRows) {
                while (dataRead.Read()) {
                    result = Convert.ToDateTime(dataRead["dateTime"]);
                }
            }
            return result;
        }
        
        public string queryExecution(string query) {
            string result = "";

            return result;
        }

        public string contitionWhereDynamically(Dictionary<string, string> condition) {
            string where = @"";

            try {
                foreach (var value in condition) {
                    //1 for Key  1 for Value  
                    where += value + " " + value + " AND";
                }
            } catch (Exception ex) {
                DM0312_ErrorLog.RegistraError(
                    MethodBase.GetCurrentMethod().Name.ToString(),
                    MethodBase.GetCurrentMethod().DeclaringType.ToString(), ex);
                MessageBox.Show(ex.Message);
            }

            if (where.Substring(where.Length - 3) == "AND") {
                where = "Where " + where.Remove(where.Length - 3, 3);
            }

            return where;
        }

        public void SPID() {
            SqlDataReader dr = null;

            try {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = ClaseEstatica.ConexionEstatica;
                cmd.CommandText = "select @@SPID";
                cmd.CommandType = CommandType.Text;

                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                    while (dr.Read()) {
                        ClaseEstatica.SPID = dr[0].ToString();
                    }
            } catch (Exception ex) {
                DM0312_ErrorLog.RegistraError(
                    MethodBase.GetCurrentMethod().Name.ToString(),
                    MethodBase.GetCurrentMethod().DeclaringType.ToString(), ex);
                MessageBox.Show(ex.Message);
            } finally {
                if (dr != null)
                    dr.Close();
            }
        }
    }
}