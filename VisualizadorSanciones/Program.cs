﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisualizadorSanciones.app.view;
using VisualizadorSanciones.config;

namespace VisualizadorSanciones {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            conexion Conexion = new conexion();
            Conexion.SqlConexionStatica();
            ClaseEstatica.ConexionEstatica.Open();

            Conexion.SqlConexionStaticaAndroid();
            ClaseEstatica.ConexionEstaticaAndroid.Open();

            //-Venta Cruzada
            //Conexion para agregar registros a ServicioAndroid
            Conexion.SqlConexionStaticaAndroidServicio();
            ClaseEstatica.ConexionEstaticaAndroidS.Open();

            //-ReporteDima
            Conexion.SqlConexionStaticaAndroidSid();
            ClaseEstatica.ConexionEstaticaAndroidSid.Open();
            
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new mainSanctionViewer());
        }
    }
}